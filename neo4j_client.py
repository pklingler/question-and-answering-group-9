from neo4j_queries import lat_query, AFFILIATION_INTENT, WIN_LOSS_LAT

class Expert:
    def __init__(self, neo4j_graph):
        self.neo4j_graph = neo4j_graph

    def process_wit_resp(self, wit_resp):
        def get_values(arr):
            return [d['value'] for d in arr] if arr else {}
        
        def get_best_value(arr):
            return arr[0]['value']

        self.text = wit_resp['_text']
        entities = wit_resp['entities']
        self.team_ents = get_values(entities.get('team'))
        if self.team_ents:
            self.team_ents = {"team"+str(i+1): v for i, v in enumerate(self.team_ents)}
        self.player_ents = get_values(entities.get('player'))

        if self.player_ents:
            self.player_ents = {"player"+str(i+1): v for i, v in enumerate(self.player_ents)}

        self.intent = get_best_value(entities['intent'])
        self.lat = get_best_value(entities['lat'])
        self.agg_type = get_best_value(entities['aggregate_type'])
        self.agg_func = get_best_value(entities['aggregate_function'])
    
    def format_response(self, resp):
        f = lambda x: list(dict(x).values())[0]
        if resp.iloc[:, 0].dtype != "object":
            f = lambda x: str(x)

        return '\n'.join(resp.iloc[:, 0].apply(f).values)

    def ask(self, wit_resp):
        self.process_wit_resp(wit_resp)

        if self.lat == WIN_LOSS_LAT:
            query = lat_query[self.lat]
        else:
            query = lat_query[self.lat][self.intent]

            if self.intent != AFFILIATION_INTENT and self.agg_func:
                query = query[self.agg_func][self.agg_type]

        query = query.format(**self.team_ents, **self.player_ents)

        return self.format_response(self.neo4j_graph.run(query).to_data_frame())
