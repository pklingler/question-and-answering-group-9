## Running the QA system
First, make sure neo4j is running. 
Then, just run `python wit_client.py`

## Running Neo4j with Docker
First, pull the docker image
```
docker pull neo4j
```

Then, run it:
```
docker run \
    -p7474:7474 -p7687:7687 \
    -v $PWD/neo4j/data:/data \
    neo4j
```

If authentication fails, go to `localhost:7474` to change your password from `neo4j` to `password`

## Target Questions
**Entities**: Team, Player, Game
1. How many times did Team play Team?
2. How many points did Player score at Game?
3. Who won Game?
4. How many rebounds did Player make this season?
5. Which Player(s) scored in Game?
6. What is the average score of Player/Team per game?
7. What is Team's win/loss record?
8. Who(which Player) scored the most points in Game?
9. When did Team play Team?
10. Who does Player play for?
11. Who plays for Team?
12. Who is the top scorer on Team?
13. How many points did Player score against Team?
14. Which Team has the most rebounds?

