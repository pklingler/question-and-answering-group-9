from wit import Wit
from neo4j_client import Expert
from py2neo import Graph
from auth import NEO4J_HOST, NEO4J_PASSWORD, WIT_TOKEN
from time import sleep

def qa(wit_token, neo4j_graph):
    wit_client = Wit(wit_token)
    print('Connected to wit.ai.')
    
    print('Ask questions about the 2018 NBA season. Type "done" to finish')
    q = input('Q: ')

    while q != "done":
        print(q)

        expert = Expert(neo4j_graph)
        ans = expert.ask(wit_client.message(q))
        sleep(.5) # trying to keep request rate to 1 per second

        print('A:')
        print(ans)
        print()
        print('Q: ', end="")
        q = input()
    print("done")



if __name__ == "__main__":
    neo4j_graph = Graph(f"bolt://neo4j:{NEO4J_PASSWORD}@{NEO4J_HOST}")
    qa(WIT_TOKEN, neo4j_graph)