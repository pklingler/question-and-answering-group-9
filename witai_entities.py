import requests
import json
from auth import AUTH_TOKEN
from itertools import combinations
import time
from tqdm import tqdm
import os

# example curl request
#   $ curl -XPOST 'https://api.wit.ai/entities/favorite_city/values?v=20170307' \
#   -H 'Authorization: Bearer $TOKEN' \
#   -H 'Content-Type: application/json' \
#   -d '{"value":"London",
#        "expressions":["London"],
#        "metadata":"CITY_1234"}'

headers = {
    "Authorization": "Bearer " + AUTH_TOKEN,
    "Content-Type": "application/json"
}

url = "https://api.wit.ai/entities/{}/values?v=20170307"

NBA_TEAM = "team"
NBA_PLAYER = "player"
GAME = "game"

team_url = url.format(NBA_TEAM)
player_url = url.format(NBA_PLAYER)
game_url = url.format(GAME)

def team_expressions(team):
    expressions = set([team, *team.split()])
    expressions |= set([' '.join(c) for c in combinations(team.split(), 2)])
    return list(expressions)

def player_expressions(player):
    return [player, *player.split()]

def upload_roster(roster):
    for team in roster:
        response = requests.post(
            team_url, 
            json={
                "value": team,
                "expressions": team_expressions(team)
            }, 
            headers=headers)

        print(f"Uploading players for {team}")
        for player in tqdm(roster[team]):
            response = requests.post(
                player_url, 
                json={
                    "value": team,
                    "expressions": player_expressions(player)
                }, 
                headers=headers)

if __name__ == "__main__":
    with open('rosters.json') as mf:
        upload_roster(json.load(mf))
