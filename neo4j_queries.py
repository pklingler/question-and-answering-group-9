WIN_LOSS_LAT = "W/L record"
PLAYER_LAT = "Player"
TEAM_LAT = "Team"
GAME_LAT = "Game"
GAMES_PLAYED_LAT = "Games Played"
REBOUNDS_LAT = "rebounds"
SCORE_LAT = "score"

AFFILIATION_INTENT = "affiliation"
GAME_STATS_INTENT = "game stats"
TEAM_STATS_INTENT = "team stats"
PLAYER_STATS_INTENT = "player stats"

GAMES_PLAYED_TYPE = "games played"
POINTS_TYPE = "points"
REBOUNDS_TYPE = "rebounds"

COUNT = "count"
SUM = "sum"
NONE = "None"
MAX = "max"
MIN = "min"
AVERAGE = "average"

# dictionary of dictionaries with:
# LAT->INTENT->AGG_FUNC->AGG_TYPE as keys in that order
lat_query = {
    WIN_LOSS_LAT: """
        MATCH (t1:Team {{name: "{team1}"}})-[p1:Played]-(game:Game)-[p2:Played]-(t2:Team)
        with p1.score - p2.score as diff
        with count(*) as games, length(filter(x in collect(diff)  WHERE x > 0)) AS wins
        return wins,games - wins as losses
    """,
    PLAYER_LAT: {
        AFFILIATION_INTENT: 'MATCH (p1:Player)-[:`Plays for`]-(:Team {{name: "{team1}"}}) return p1',
        PLAYER_STATS_INTENT: {
            MAX: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(player:Player)-[:`Plays for`]-(:Team {{name: "{team1}"}})
                    WITH player, sum(played.points) AS points
                    return player, points
                    ORDER BY points DESC
                    LIMIT 1
                """,
                REBOUNDS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(player:Player)-[:`Plays for`]-(:Team {{name: "{team1}"}})
                    WITH player, sum(played.rebounds) AS rebounds
                    return player, rebounds
                    ORDER BY rebounds DESC
                    LIMIT 1
                """
            }
        }
    },
    TEAM_LAT: {
        AFFILIATION_INTENT: 'MATCH (:Player {{name: "{player1}"}})-[:`Plays for`]-(t1:Team) return t1'
    },
    GAME_LAT: {
        GAME_STATS_INTENT: {
            NONE: {
                NONE: 'MATCH (:Team {{name: "{team1}"}})-[:Played]-(g:Game)-[:Played]-(:Team {{name:"{team2}"}}) return g'
            }
        }
    },
    GAMES_PLAYED_LAT: {
        TEAM_STATS_INTENT: {
            COUNT: {
                GAMES_PLAYED_TYPE: 'MATCH (:Team {{name: "{team1}"}})-[:Played]-(g:Game)-[:Played]-(:Team {{name:"{team2}"}}) return count(*)'
            }
        }
    },
    REBOUNDS_LAT: {
        PLAYER_STATS_INTENT: {
            SUM: {
                REBOUNDS_TYPE: 'MATCH (player:Player {{name: "{player1}"}})-[p:Played]-(:Game) return sum(p.rebounds)'
            },
            MAX: {
                REBOUNDS_TYPE: 'MATCH (player:Player {{name: "{player1}"}})-[p:Played]-(:Game) return max(p.rebounds)'
            },
            MIN: {
                REBOUNDS_TYPE: 'MATCH (player:Player {{name: "{player1}"}})-[p:Played]-(:Game) return min(p.rebounds)'
            },
            AVERAGE : {
                REBOUNDS_TYPE: """
                    MATCH (player:Player {{name: "{player1}"}})-[p:Played]-(:Game)
                    WITH 10^2 AS factor, avg(p.rebounds) as avg_rebounds
                    RETURN round(factor * avg_rebounds)/factor AS avg_rebounds"""
            }
        },
        TEAM_STATS_INTENT: {
            SUM: {
                REBOUNDS_TYPE: 'MATCH (player1:Player)-[p1:Played]-(:Game)-[p2:Played]-(team1:Team {{name: "{team1}"}}) return sum(p1.rebounds)'
            },
            MAX: {
                REBOUNDS_TYPE: """
                    MATCH (team1:Team {{name: "{team1}"}})-[:`Plays for`]-(player1:Player)-[p1:Played]-(g:Game)
                    WITH g, sum(p1.rebounds) as rebounds
                    return max(rebounds)
                """
            },
            MIN: {
                REBOUNDS_TYPE: """
                    MATCH (team1:Team {{name: "{team1}"}})-[:`Plays for`]-(player1:Player)-[p1:Played]-(g:Game)
                    WITH g, sum(p1.rebounds) as rebounds
                    return min(rebounds)
                """
            },
            AVERAGE: {
                REBOUNDS_TYPE: """
                    MATCH (team1:Team {{name: "{team1}"}})-[:`Plays for`]-(player1:Player)-[p1:Played]-(g:Game)
                    WITH 10^2 AS factor, avg(rebounds) as avg_rebounds
                    RETURN round(factor * avg_rebounds)/factor AS avg_rebounds
                """                   
            }
        }
    },
    SCORE_LAT: {
        TEAM_STATS_INTENT: {
            MAX: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Team {{name: "{team1}"}})
                    RETURN max(played.score)
                """
            },
            MIN: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Team {{name: "{team1}"}})
                    RETURN min(played.score)
                """
            },
            SUM: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Team {{name: "{team1}"}})
                    RETURN sum(played.score)
                """
            },
            AVERAGE: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Team {{name: "{team1}"}})
                    WITH 10^2 AS factor, avg(played.score) as avg_score
                    RETURN round(factor * avg_score)/factor AS avg_score_
                """
            }
        },
        PLAYER_STATS_INTENT: {
            MAX: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Player {{name: "{player1}"}})
                    RETURN max(played.points)
                """
            },
            MIN: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Player {{name: "{player1}"}})
                    RETURN min(played.points)
                """
            },
            SUM: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Player {{name: "{player1}"}})
                    RETURN sum(played.points)
                """
            },
            AVERAGE: {
                POINTS_TYPE: """
                    MATCH (game:Game)-[played:Played]-(:Player {{name: "{player1}"}})
                    WITH 10^2 AS factor, avg(played.points) as avg_points
                    RETURN round(factor * avg_points)/factor AS avg_points
                """
            }
        }
    }
}
